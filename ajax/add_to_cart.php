<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");?>
<?
$_POST["id"];
if(!isset($_SESSION["CART_ITEMS"]))
{
    $_SESSION["CART_ITEMS"] = array();
}
if (array_key_exists($_POST["id"], $_SESSION["CART_ITEMS"]))    
{
    $_SESSION["CART_ITEMS"][$_POST["id"]] = $_SESSION["CART_ITEMS"][$_POST["id"]] + $_POST["count"];
}
else
{
    $_SESSION["CART_ITEMS"][$_POST["id"]] = $_POST["count"];
}
$APPLICATION->IncludeFile(
    SITE_DIR."include/top_cart.php",
    Array(),
    Array("MODE"=>"text")
);
?>