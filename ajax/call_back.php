<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
if(CModule::IncludeModule("iblock")) {
    $result['status'] = 1;
    $IBLOCK_ID = 19;
    $result['message'] = "";
    foreach ($_POST as $key => $value) {
        if ($value) {
            $sendArray[$key] = htmlspecialcharsbx($value);
        } else {                 
            if($key=="namee")
            {
                $result['status'] = 0;
                $result['message'].= 'Не заполнено поле Имя<br />';
            }
        }
    }
    if(($sendArray['phonee']=="")&&($sendArray['maill']==""))
    {
        $result['status'] = 0;
        $result['message'].= 'Необходимо заполнить поле Телефон или поле E-mail<br />';
    }
    if ($result['status'] != 0) {
        $el = new CIBlockElement;
        
        $preview_text = "Имя: ".$sendArray['namee'];
        if($sendArray['phonee'])
        {
            $preview_text.= "\n\rТелефон: ".$sendArray['phonee'];
        }
        if($sendArray['maill'])
        {
            $preview_text.= "\n\rE-mail: ".$sendArray['maill'];
        }
        if($sendArray['quastionn'])
        {
            $preview_text.= "\n\rТекст вопроса: ".$sendArray['quastionn'];
        }
        $name = "Заявка от ".date('Y-m-d H:i');
        $arLoadProductArray = Array(
            "IBLOCK_SECTION_ID" => false,
            "IBLOCK_ID"      => $IBLOCK_ID,
            "NAME"           => $name,
            "ACTIVE"         => "Y",
            "PREVIEW_TEXT"   =>  $preview_text,
        );

        if($PRODUCT_ID = $el->Add($arLoadProductArray))
        {
            $result['status'] = 1;
            $result['message'] = '';
            CEvent::Send("FORM_CALL_BACK", 's2', array(
                "NAME"=>$sendArray['namee'], 
                "PHONE"=>$sendArray['phonee'],                    
                "MAIL"=>$sendArray['maill'],                    
                "QUASTION"=>$sendArray['quastionn'],                    

            ));
        } 
        else 
        {
            $result['status'] = 0;
            $result['message'] = 'Произошла ошибка! Попробуйте позже';
            //$result['debag'] = $el->LAST_ERROR;
        }
    }
    
    echo json_encode($result);
} 
?>