<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Компании");
?><h1>Корпоративные заказы</h1>
<p>
 <br>
	 Мы разрабатываем по вашим заказам изделия из шоколада и марципана с индивидуальным дизайном. Это могут быть шоколадные фигуры, конфеты и марципан в упаковке.
</p>
<p>
 <br>
</p>
<p>
</p>
<table border="2" cellpadding="10" cellspacing="1">
<tbody>
<tr>
	<td style="text-align: center;">
		 &nbsp;&nbsp;<b>№&nbsp;&nbsp;</b>
	</td>
	<td style="text-align: center;">
 <b>Продукт и упаковка </b>
	</td>
	<td style="text-align: center;">
 <b>&nbsp;Минимальная&nbsp;<br>
 </b><b>сумма<br>
 </b><b>заказа</b>
	</td>
</tr>
<tr>
	<td style="text-align: center;">
		 &nbsp; &nbsp;1&nbsp; &nbsp; &nbsp;&nbsp;
	</td>
	<td style="text-align: center;">
		<p style="text-align: left;">
			 Наш продукт из постоянных линеек<br>
 <a title="Образцы регулярной коллекции" href="/products/">Варианты здесь &gt;</a>
		</p>
	</td>
	<td style="text-align: center;">
		 &nbsp; &nbsp;250 000₽&nbsp; &nbsp;<br>
	</td>
</tr>
<tr>
	<td style="text-align: center;">
		 &nbsp; &nbsp;2&nbsp; &nbsp; &nbsp;&nbsp;
	</td>
	<td style="text-align: center;">
		<p style="text-align: left;">
			 Наш продукт из нерегулярного ассортимента<br>
 <a title="Корпоративные (индивидуальные) заказы" href="http://www.fabrica-aztec.com/products/korporativ-zakazy/">Варианты здесь &gt;&gt;</a>&nbsp;<br>
		</p>
	</td>
	<td style="text-align: center;">
		 &nbsp; &nbsp;300 000₽&nbsp; &nbsp;
	</td>
</tr>
<tr>
	<td style="text-align: center;">
		 &nbsp; &nbsp;3&nbsp; &nbsp;&nbsp;
	</td>
	<td style="text-align: center;">
		<p style="text-align: left;">
			 Продукт "с нуля" по вашему проекту,&nbsp;с учетом наших технических возможностей<br>
		</p>
	</td>
	<td style="text-align: center;">
		 &nbsp; &nbsp;300 000₽*&nbsp;
	</td>
</tr>
</tbody>
</table>
<p>
 <i>&nbsp; * без стоимости разработки проекта, необходимых форм и дизайна упаковки</i>
</p>
<p>
</p>
<p>
</p>
<p>
 <br>
</p>
<p>
	 Уточнить условия и обсудить детали заказа вы можете по телефону: <b>+7 (812) 677-69-61 </b>или по электронной почте <a href="mailto:info@fabrica-aztec.com">info@fabrica-aztec.com</a>.
</p><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>