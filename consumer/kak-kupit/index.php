<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Как купить");
?><h1>Как купить?</h1>
 <br>
 <br>
 Приобрести нашу продукцию&nbsp;можно одним из удобных способов:<br>
 <br>
 <br>
 1. <b>Интернет-заказ</b><br>
 <br>
 &nbsp; &nbsp; Просто добавляйте <a title="Каталог продукции" href="/products/">продукцию из каталога</a>&nbsp;в корзину и отправляйте&nbsp;онлайн-заявку.&nbsp;<br>
 &nbsp; &nbsp; С вами свяжется наш менеджер, расскажет об условиях оплаты и доставки заказа.<br>
 <br>
 <br>
 2. <b>Розничные сети</b><br>
 <br>
 &nbsp; &nbsp; Спрашивайте продукцию в&nbsp;<a href="http://www.fabrica-aztec.com/distributor/torgovye-seti/">магазинах и&nbsp;торговых&nbsp;сетях</a>&nbsp;своего города.<br>
 <br>
 <br>
 3. <b>Ф</b><b>ирменный</b><b>&nbsp;магазин&nbsp;Grondard в Санкт-Петербурге</b><br>
 <br>
<ul>
	<li><b>Универмаг "Гостиный двор"</b><br>
	 Невский проспект, 35<br>
	Невская линия,&nbsp;1-й этаж<br>
	 ежедневно с 10:00 до 22:00</li>
</ul><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>