<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("О брендах");
?><h1>О брендах</h1>
 <br>
 Фабрика работает с 2001 года и с момента основания специализируется на производстве марципана, конфет и шоколадных фигур. Наш флагманский бренд Grondard – лидер рынка марципана в России развивается с 2010 года. В 2017 году у нас появилась новая марка Mario &amp; Bianca – это конфеты из орехов, злаков, ягод и фруктов.<br>
 <br>
 Узнать больше о продукции Вы можете на официальных сайтах наших брендов:<br>
 <br>
 <br>
<div class="row brends-list">
    <div class="col-xs-12 col-sm-4">
        <a title="Grondard Марципан и марципановые конфеты" target="_blank" href="http://grondard.com/"><img src="/consumer/brends_grondard.jpg" class="img-responsive" /></a><br />
        <a title="Grondard Марципан и марципановые конфеты" href="http://grondard.com/" target="_blank">grondard.com</a>
    </div>
    <div class="col-xs-12 col-sm-4">
        <a href="http://mariobianca.com/" title="Mario &amp; Bianca Конфеты из орехов, злаков, ягод и фруктов" target="_blank"><img src="/consumer/brends_mariobianca.jpg" class="img-responsive" /></a><br />
        <a title="Mario &amp; Bianca Конфеты из орехов, злаков, ягод и фруктов" href="http://mariobianca.com/" target="_blank">mariobianca.com</a>
    </div>
    <div class="col-xs-12 col-sm-4">
        <a href="http://magicmarzipan.ru/" title="Волшебный марципан Необыкновенное лакомство" target="_blank"><img src="/consumer/brends_magicmarzipan.jpg" class="img-responsive" /></a><br />
        <a title="Волшебный марципан Необыкновенное лакомство" href="http://magicmarzipan.ru/" target="_blank">magicmarzipan.ru</a>	
    </div>
</div>
 <br>
 <br>
 Будем рады Вашим отзывам, идеям и пожеланиям. Чтобы отправить их нам, можете воспользоваться <a href="javascript:void(0);" data-toggle="modal" data-target=".bs-example-modal-sm">формой обратной связи</a>. Или звоните по телефону: +7 (812) 677-69-61.<br><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>