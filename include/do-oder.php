<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?> 
<?
//ppr($_POST);
$name = strip_tags($_POST["name"]);
$phone = strip_tags($_POST["phone"]);
$email = strip_tags($_POST["email"]);
$comment = strip_tags($_POST["comment"]);
$zakaz = "";
$count_price = 0;
foreach($_POST["item-name"] as $elementKey => $elementValue)
{
    $zakaz.= $elementValue." - ".$_POST["item-count"][$elementKey]." шт. ( цена за шт ".$_POST["item-price"][$elementKey]." руб.)\n";
    $count_price = $count_price + ($_POST["item-count"][$elementKey]*$_POST["item-price"][$elementKey]);
}
$zakaz.= "Итого: ".$count_price." руб.";
//echo $zakaz;
$el = new CIBlockElement;
$title = "Заявка от ".date('Y-m-d H:i');
$PROP = array();
$PROP["NAME"] = $name; 
$PROP["PHONE"] = $phone;
$PROP["MAIL"] = $email;
$PROP["COMMENT"] = $comment;
$arLoadProductArray = Array(
    "IBLOCK_SECTION_ID" => false,
    "IBLOCK_ID"      => 23,
    "NAME"           => $title,
    "ACTIVE"         => "Y",
    "PREVIEW_TEXT"   =>  $zakaz,
    "PROPERTY_VALUES"=> $PROP,
);
if($PRODUCT_ID = $el->Add($arLoadProductArray))
{
    CEvent::Send("ZAKAZ", 's2', array(
        "NAME"=>$name, 
        "PHONE"=>$phone,                    
        "EMAIL"=>$email,                    
        "COMMENT"=>$comment,                    
        "ZAKAZ"=>$zakaz, 
        "ZAKAZ_ID"=> $PRODUCT_ID,                  
    ));
    if($email!="")
    {
        CEvent::Send("ZAKAZ_USER", 's2', array(
            "NAME"=>$name, 
            "PHONE"=>$phone,                    
            "EMAIL"=>$email,                    
            "COMMENT"=>$comment,                    
            "ZAKAZ"=>$zakaz, 
            "ZAKAZ_ID"=> $PRODUCT_ID,                  
        ));        
    }
} 
//echo "<span class='green'>Спасибо ваш заказ принят, номер вашего заказа №".$PRODUCT_ID."</span>";
?>
    <p>
    Ваша заявка принята, номер заказа: <b>№ <?=$PRODUCT_ID;?></b>.<br />
    Менеджер интернет-магазина свяжется с вами в течение дня: подтвердит заказ и расскажет, когда и как вы сможете его получить. 
    </p>
    <p>
    Внимание: мы работаем с понедельника по пятницу с 10:00 до 18:00 (московское время).<br />
    Заказы, оформленные в будни после 16:00 и в выходные, обрабатываются утром ближайшего рабочего дня.<br />
    Если вам не перезвонили – свяжитесь с нами,пожалуйста, по e-mail: <a href="mailto:ishop@fabrica-aztec.com.">ishop@fabrica-aztec.com.</a>
    </p>
    <p>
    Спасибо за покупку и выбор кондитерской фабрики «Ацтек»!
    </p>
<?
unset($_SESSION["CART_ITEMS"]);
?>