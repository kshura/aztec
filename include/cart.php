<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");?>
<?
$curs = "1";
if(count($_SESSION["CART_ITEMS"])<1)
{
    unset($_SESSION["CART_ITEMS"]);
    ?>
    <div class="row">
        <div class="col-xs-12 cart-empty">
            <br />
            <br />
            Корзина пуста
            <br />
            <br />
            <br />
            <a href="/products/">Вернуться в каталог продукции >></a>
        </div>
    </div>
    <script>
        $( document ).ready(function() {
            $(".cart-text").html("");
            $(".cart-form-div").html("");
            $.ajax({
                type: "POST",
                data: "", 
                url: '/ajax/refresh_mobile_cart.php',     
                success: function (html) {                                         
                    $(".cart-mobile-div").html(html);
                }
            });
            $.ajax({
                type: "POST",
                data: "", 
                url: '/ajax/refresh_top_cart.php',     
                success: function (html) {                                         
                        $(".top-cart").html(html);
                    }
            });
        });
    </script>
    <?
}
else
{
//ppr($_SESSION["CART_ITEMS"]);
?>
<table class="cart-items">
    <?
    $summ = 0;
    $cart_items = array();
    foreach($_SESSION["CART_ITEMS"] as $key=>$value):
        $cart_items[] = $key;
    endforeach;
    $arSelect = Array("ID", "NAME", "SORT", "PREVIEW_PICTURE", "DETAIL_PAGE_URL", "PROPERTY_PRICE");
    $arFilter = Array("ID"=>$cart_items, "ACTIVE"=>"Y");    
    $res = CIBlockElement::GetList(Array("sort"=>"asc"), $arFilter, false, Array("nPageSize"=>1000), $arSelect);
    $i=0;
    while($ob = $res->GetNextElement())
        {
            $arFields = $ob->GetFields();
            ?>
            <tr>
                <td class="element-pic">
                    <img class="img-responsive" src="<?=CFile::GetPath($arFields["PREVIEW_PICTURE"]);?>" alt="<?=$arFields["NAME"]?>" />
                </td>
                <td class="element-name">
                    <a href="<?=$arFields["DETAIL_PAGE_URL"]?>">
                        <?=$arFields["NAME"]?>
                    </a>
                </td>
                <td class="element-price">
                    <?
                    $price = ceil($arFields["PROPERTY_PRICE_VALUE"]*$curs);
                    if($arFields["IBLOCK_ID"]==7)
                    {
                        $price = $arFields["PROPERTY_PRICE_VALUE"];
                    }
                    ?>
                    <span><?=number_format(ceil($price), 0, '', ' ');?></span> руб.
                    <?
                    $summ = $summ + (ceil($price)*$_SESSION["CART_ITEMS"][$arFields["ID"]]);
                    ?>
                </td>
                <td class="element-cont">
                    <div>
                        <span class="minus">-</span>
                        <input name="item-count[<?=$i?>]" type="text" class="item-count" data-item-id="<?=$arFields["ID"]?>" value="<?=$_SESSION["CART_ITEMS"][$arFields["ID"]]?>" autocomplete="off" onkeypress="validate(event)" />
                        <input name="item-name[<?=$i?>]" value="<?=$arFields["NAME"]?>" type="hidden">
                        <input name="item-price[<?=$i?>]" value="<?=number_format(ceil($price), 0, '', ' ');?>" type="hidden">
                        <span class="plus">+</span>
                    </div>
                </td>
                <td class="element-del">
                    <a href="" class="del fa fa-close" data-item-id="<?=$arFields["ID"]?>"> </a>
                </td>
            </tr>
            <?
            //ppr($arFields);
            $i++;
        }
    ?>
</table>
<br />
        <div class="cart-summ">Итого: <span><?=number_format($summ, 0, '', ' ');?></span> руб.</div>
<?
}
?>