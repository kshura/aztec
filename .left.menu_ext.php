<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

global $APPLICATION;

$aMenuLinksExt=$APPLICATION->IncludeComponent("bitrix:menu.sections", "", array(
   "IS_SEF" => "Y",
   "IBLOCK_TYPE" => "catalog_aztec",
   "IBLOCK_ID" => "14",
   "SECTION_URL" => "",
   "DEPTH_LEVEL" => "4",
   "CACHE_TYPE" => "A",
   "CACHE_TIME" => "3600"
   ),
   false
);

$aMenuLinks = array_merge($aMenuLinks, $aMenuLinksExt);
?>