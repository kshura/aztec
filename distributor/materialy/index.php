<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Материалы");
?><h1>Материалы&nbsp;</h1>
 <br>
 Уважаемые коллеги, здесь вы можете посмотреть и скачать рабочую информацию о фабрике и нашей продукции.<br>
 <br>
 <br>
<div class="row brends-list">
	<div class="col-xs-12 col-sm-4">
 <a href="/products/aztec_catalog.pdf" title="Каталог продукции АЦТЕК" target="_blank"><img src="/distributor/materialy/aztec_catalog_200.jpg" title="Каталог продукции АЦТЕК" class="img-responsive"></a><br>
 <a title="Каталог продукции АЦТЕК" href="/products/aztec_catalog.pdf" target="_blank">Каталог<br>
		 продукции<br>
		 "Ацтек"</a>
	</div>
	<div class="col-xs-12 col-sm-4">
 <a title="Новогодняя коллекция АЦТЕК 2019 марципан шоколад конфеты" href="/distributor/materialy/aztec_winter2019.pdf" target="_blank"><img src="/distributor/materialy/aztec_zima_200.jpg" class="img-responsive"></a><br>
 <a title="Новогодняя коллекция АЦТЕК 2019" href="/distributor/materialy/aztec_winter2019.pdf" target="_blank">Новогодняя<br>
		 коллекция<br>
		 2019</a>
	</div>
	<div class="col-xs-12 col-sm-4">
 <a title="Весенняя коллекция АЦТЕК 2019" href="/distributor/materialy/aztec_spring2019.pdf" target="_blank"><img src="/distributor/materialy/aztec_vesna_200.jpg" class="img-responsive"></a><br>
 <a title="Весенняя коллекция АЦТЕК 2019" href="/distributor/materialy/aztec_spring2019.pdf" target="_blank">Весенняя<br>
		 коллекция<br>
		 2019</a>
	</div>
</div>
<br><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>