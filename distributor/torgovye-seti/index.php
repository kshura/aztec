<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Торговые сети");
?><h1>Торговые сети и магазины</h1>
 Наши партнёры:&nbsp;магазины и торговые сети, в которых представлена продукция кондитерской фабрики "Ацтек":<br>
 <br>
<?$APPLICATION->IncludeComponent(
	"bitrix:catalog.section.list",
	"accord-torg",
	Array(
		"ADD_SECTIONS_CHAIN" => "Y",
		"CACHE_GROUPS" => "Y",
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "A",
		"COMPONENT_TEMPLATE" => "accord",
		"COUNT_ELEMENTS" => "Y",
		"IBLOCK_ID" => "22",
		"IBLOCK_TYPE" => "distributor",
		"SECTION_CODE" => "",
		"SECTION_FIELDS" => array(0=>"",1=>"",),
		"SECTION_ID" => "",
		"SECTION_URL" => "",
		"SECTION_USER_FIELDS" => array(0=>"",1=>"",),
		"SHOW_PARENT_NAME" => "Y",
		"TOP_DEPTH" => "2",
		"VIEW_MODE" => "LIST"
	)
);?><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>