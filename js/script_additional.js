
function validate(evt) {
  var theEvent = evt || window.event;
  var key = theEvent.keyCode || theEvent.which;
  key = String.fromCharCode( key );
  var regex = /[0-9]|\./;
  if( !regex.test(key) ) {
    theEvent.returnValue = false;
    if(theEvent.preventDefault) theEvent.preventDefault();
  }
}
$(document).ready(function(){
    $('.call-back').submit(function() {
        var data = $('.call-back').serialize();
        $.ajax({
              url: '/ajax/call_back.php',
              type: 'POST',
              dataType: 'json',
              data: data,
            })
            .done(function(answer) {
              $('.float-form .wrong-block').html("<div>" + answer.message + "</div>");
              if (answer.status == 1) {
                $(".call-back")[0].reset();
                $('.bs-example-modal-sm').modal('hide');
                $('.modal-ok').modal('show');
              }
            })
            .fail(function() {
              console.log("error");
            }) ;

        return false;
    });
    $("html").on( "click", ".counter_block .plus", function() {
      $(this).prev().val(parseInt($(this).prev().val())+1);
    });
    $("html").on( "click", ".counter_block .minus", function() {
      if(parseInt($(this).next().val())>1)
      {
        $(this).next().val(parseInt($(this).next().val())-1);
      }
    });
    $( ".add-to-cart" ).click(function() {
        var obj = $(this);
        var count = $(this).parent().prev().find("input").val();
        //alert(count);
        $.ajax({
            type: "POST",
            data: "id=" + $(this).attr("data-product-id") + "&count=" + count, 
            url: '/ajax/add_to_cart.php',     
            success: function (html) {                                         
                    $(".top-cart").html(html);
                    if($(obj).attr("data-detail")=="1")
                    {
                        var that = $(obj).parent().parent().parent().prev().find('.img img');
                    }
                    else
                    {
                        var that = $(obj).parent().parent().find('.img img');
                    }                    
                    var bascket = $(".top-cart");
                    var w = that.width();
                    //alert(that.offset().top);
                        that.clone()
                            .css({'width' : w,
                            'position' : 'absolute',
                            'z-index' : '9999',
                            top: that.offset().top,
                            left:that.offset().left})
                                  .appendTo("body")
                                  .animate({opacity: 0.05,
                                      left: bascket.offset()['left'],
                                      top: bascket.offset()['top'],
                                      width: 20}, 1000, function() {	
                                $(this).remove();
                              });
                }
            });
            setTimeout(
                function() {
                    $.ajax({
                        type: "POST",
                        data: "", 
                        url: '/ajax/refresh_mobile_cart.php',     
                        success: function (html) {                                         
                            $(".cart-mobile-div").html(html);
                        }
                    });                
                }, 500);

    });

    $("html").on( "keyup", ".item-count", function() {
        //alert($(this).val());
         $.ajax({
            type: "POST",
            data: "action=update&id=" + $(this).attr("data-item-id") + "&value=" + $(this).val(), 
            url: '/ajax/update_cart.php',     
            success: function (html) {                                         
                    $(".cart-div").html(html);
                }
            });
            setTimeout(
                function() {
                        $.ajax({
                            type: "POST",
                            data: "", 
                            url: '/ajax/refresh_top_cart.php',     
                            success: function (html) {                                         
                                    $(".top-cart").html(html);
                                }
                            });
                            $.ajax({
                                type: "POST",
                                data: "", 
                                url: '/ajax/refresh_mobile_cart.php',     
                                success: function (html) {                                         
                                    $(".cart-mobile-div").html(html);
                                }
                            }); 
                    }, 500);
    });
    $("html").on( "click", ".cart-items a.del", function(event) {
        event.preventDefault();
        $.ajax({
            type: "POST",
            data: "action=delete&id=" + $(this).attr("data-item-id"), 
            url: '/ajax/update_cart.php',     
            success: function (html) {                                         
                    $(".cart-div").html(html);
                }
            });
            
            setTimeout(
                function() {
                        $.ajax({
                            type: "POST",
                            data: "", 
                            url: '/ajax/refresh_top_cart.php',     
                            success: function (html) {                                         
                                    $(".top-cart").html(html);
                                }
                            });
                            $.ajax({
                                type: "POST",
                                data: "", 
                                url: '/ajax/refresh_mobile_cart.php',     
                                success: function (html) {                                         
                                    $(".cart-mobile-div").html(html);
                                }
                            }); 
                    }, 500);
    });
    $("html").on( "click", ".cart-items span.plus", function() {
        $.ajax({
            type: "POST",
            data: "action=plus&id=" + $(this).prev().prev().prev().attr("data-item-id"), 
            url: '/ajax/update_cart.php',     
            success: function (html) {                                         
                    $(".cart-div").html(html);
                }
            });
            setTimeout(
                function() {
                        $.ajax({
                            type: "POST",
                            data: "", 
                            url: '/ajax/refresh_top_cart.php',     
                            success: function (html) {                                         
                                    $(".top-cart").html(html);
                                }
                            });
                            $.ajax({
                                type: "POST",
                                data: "", 
                                url: '/ajax/refresh_mobile_cart.php',     
                                success: function (html) {                                         
                                    $(".cart-mobile-div").html(html);
                                }
                            }); 
                    }, 500);
    });
    $("html").on( "click", ".cart-items span.minus", function() {
        if($(this).next().val()!=1)
        {
            $.ajax({
                type: "POST",
                data: "action=minus&id=" + $(this).next().attr("data-item-id"), 
                url: '/ajax/update_cart.php',     
                success: function (html) {                                         
                        $(".cart-div").html(html);
                    }
                });
            setTimeout(
                function() {
                        $.ajax({
                            type: "POST",
                            data: "", 
                            url: '/ajax/refresh_top_cart.php',     
                            success: function (html) {                                         
                                    $(".top-cart").html(html);
                                }
                            });
                            $.ajax({
                                type: "POST",
                                data: "", 
                                url: '/ajax/refresh_mobile_cart.php',     
                                success: function (html) {                                         
                                    $(".cart-mobile-div").html(html);
                                }
                            }); 
                    }, 500);
        }
    });

    $(document).on('submit','form.cart-form-all',function(event){
        var name = $("form.cart-form-all input[name='name']").val();
        var phone = $("form.cart-form-all input[name='phone']").val();
        var email = $("form.cart-form-all input[name='email']").val();
        var comment = $("form.cart-form-all textarea").val();
        var error = '';
        if(name=='')
        {
            error = 1;
            $("form.cart-form-all input[name='name']").addClass('error'); 
        }
        if(phone=='')
        {
            error = 1;
            $("form.cart-form-all input[name='phone']").addClass('error'); 
        }
        if(error=="1")
        {
            event.preventDefault();
        }
        if(error=='')
        {
            $('form.cart-form-all').submit(); 
        }
        //if()
        //$('form.cart-form-all').submit();
    });

});