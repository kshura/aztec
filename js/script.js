function re_height_auto(work_element) {       
    $(work_element).each(function(indx, element){
        $(element).outerHeight("auto");
    });
}

function re_height(work_element) {    
    var max_height=0;    
    $(work_element).each(function(indx, element){
        if(max_height<$(element).height())
        {
            max_height=$(element).height();
        }
        //alert(max_height);
    });    
    $(work_element).each(function(indx, element){
        $(element).height(max_height);
    });
}
$(document).ready(function(){
    
    $('.pic-block img').width($('.pic-block').innerWidth());
    $(window).resize(function () {
        $('.pic-block img').width($('.pic-block').innerWidth());
    });

    

    
    $("[data-fancybox]").fancybox({
        slideShow  : false,
        fullScreen : false,
        thumbs     : false,      
    });

    $( "#horizontal-multilevel-menu li" ).click(function() {
        $(this).find(">ul").toggle( "slow", function() {       
        
        }); 
    });
    $( ".slide-sections" ).click(function() {
        $("#horizontal-multilevel-menu").toggle( "slow", function() {       
        
        }); 
    });

    $( ".accord li span" ).click(function() {
        if($(this).hasClass('akt'))
        {
            $(this).removeClass('akt');
            $(this).find('span').removeClass('spin').addClass('spin-out');
            $(this).next().slideUp('slow');
        }
        else
        {
            $(this).addClass('akt');
            $(this).find('span').addClass('spin').removeClass('spin-out');
            $(this).next().slideDown('slow');
        }
         

        // $(this).next().toggle( "slow", function() {       
        // });
    });
    $(".call-back-button").hover(
        function () {
            $(this).removeClass('out').addClass('over');
        },
        function () {
            $(this).removeClass('over').addClass('out');
        }
    );
    $(".float-form input[type=submit]").hover(
        function () {
            $(this).removeClass('out').addClass('over');
        },
        function () {
            $(this).removeClass('over').addClass('out');
        }
    );
    $(".float-form .modal-footer button").hover(
        function () {
            $(this).removeClass('out').addClass('over');
        },
        function () {
            $(this).removeClass('over').addClass('out');
        }
    );

    $("header nav>li").hover(
        function () {
            $(this).find(">ul").fadeIn('fast');
        },
        function () {
            $(this).find(">ul").fadeOut('fast');
        }
    );
    $("html").on( "click", ".lang-block .item-akt", function() {
        if($(this).hasClass("akt"))
        {
            $(".lang-block .lang-items").slideUp();
            $(this).removeClass("akt");
        }
        else
        {
            $(".lang-block .lang-items").slideDown();
            $(this).addClass("akt");
        }
        
    });
    $("html").on( "click", ".menu-opener .navbar-toggle", function() {
        if($(this).hasClass("akt"))
        {
            $(this).removeClass("akt");
            $( "header" ).animate({
                left: "-200",
                }, 500, 
                function() {
            });
        }
        else
        {
            $(this).addClass("akt");
            $( "header" ).animate({
                left: "0",
                }, 500, 
                function() {
            });
        }
    });

    $(".banners-xs-sm-md .item").height(($(".banners-xs-sm-md .item").width()/1.5) + "px");

    $(".catalog-sections .item").each(function(indx, element){
        $(this).height(($(this).width()*0.85) + "px");
    });
    $(window).resize(function () {
        $(".catalog-sections .item").each(function(indx, element){
            $(this).height(($(this).width()*0.85) + "px");
        });
        $(".banners-xs-sm-md .item").height(($(".banners-xs-sm-md .item").width()/1.5) + "px");
    });
    
    $('#carousel-example-generic').carousel({
        interval: 2500,
    });
    $('#carousel-example-generic-2').carousel({
        interval: 2500,
    });

    setTimeout(function()
    {
        $('#grid').height($('.sl img').innerHeight());
        $(window).resize(function () {
            $('#grid').height($('.sl img').innerHeight());
        });
    }, 1000);


    setTimeout(function()
        {
            if($(".slider").height()>$(".banners").height())
            {
                $(".banners").height($(".slider").height() + "px");
            }
            else
            {
                $(".slider").height($(".banners").height() + "px"); 
            }
        }
    , 300);

    
    if ($(window).width() > 750) 
    {
        setTimeout('re_height(".catalog-section .img")', 300);
        setTimeout('re_height(".catalog-section .name")', 300);
        setTimeout('re_height(".catalog-section .item")', 300);
        setTimeout('re_height(".catalog-section .text")', 300);
        setTimeout('re_height(".catalog-section .properties")', 300);
        setTimeout('re_height(".news-items .item")', 300);
        setTimeout('re_height(".news-items .name")', 300);
        setTimeout('re_height(".news-items .date")', 300);
        setTimeout('re_height(".news-items .text")', 300);
    }


    

    window.addEventListener("orientationchange", function() {

        if ($(window).width() > 750) 
        {
            setTimeout('re_height_auto(".catalog-section .img")', 300);
            setTimeout('re_height_auto(".catalog-section .name")', 300);
            setTimeout('re_height_auto(".catalog-section .item")', 300);
            setTimeout('re_height_auto(".catalog-section .text")', 300);
            setTimeout('re_height_auto(".catalog-section .properties")', 300);
                
            setTimeout('re_height(".catalog-section .img")', 400);
            setTimeout('re_height(".catalog-section .name")', 400);
            setTimeout('re_height(".catalog-section .item")', 400);
            setTimeout('re_height(".catalog-section .text")', 400);
            setTimeout('re_height(".catalog-section .properties")', 400);
        }

        if($(".slider").height()>$(".banners").height())
        {
            $(".banners").height($(".slider").height() + "px");
        }
        else
        {
            $(".slider").height($(".banners").height() + "px"); 
        }

    }, false); 

    $('.backtotop').click(function(){
        $('html, body').animate({scrollTop:0}, 'slow');
    });
    $(window).scroll(function(){
		if($(window).scrollTop()>100)
		{
				$('.scr').show("slow");
		}
		else 
		{
				$('.scr').hide("slow");
        }
	});

});