<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Корзина");
?><h1>Корзина</h1>
 <?
if(isset($_POST['sub']))
{
    $APPLICATION->IncludeFile(
        SITE_DIR."include/do-oder.php",
        Array(),
        Array("MODE"=>"php")
    );   
}
?>
<form action="" autocomplete="off" class="cart-form-all" method="post">
	<div class="row">
		<div class="col-xs-8 cart-div">
			 <?	
    $APPLICATION->IncludeFile(
        SITE_DIR."include/cart.php",
        Array(),
        Array("MODE"=>"php")
    );
    ?>
		</div>
		 <?
    if(count($_SESSION["CART_ITEMS"])>0)
    {
    ?>
		<div class="col-xs-4 cart-form-div">
			<div class="cart-form">
				<div class="item">
 <input type="text" name="name" autocomplete="off" placeholder="Ваше имя">
				</div>
				<div class="item">
 <input type="text" name="phone" autocomplete="off" placeholder="Телефон">
				</div>
				<div class="item">
 <input type="text" name="email" autocomplete="off" placeholder="E-mail">
				</div>
				<div class="item">
 <textarea name="comment" placeholder="Комментарий"></textarea>
				</div>
				<div class="item">
 <input type="submit" value="Заказать" name="sub">
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12 cart-text">
					<p>
						После отправки заявки менеджер интернет-магазина в течение дня свяжется с вами, подтвердит заказ и расскажет, когда и как вы сможете его получить.
					</p>
					<p>
						 Мы работаем в Санкт-Петербурге, с понедельника по пятницу с 10:00 до 18:00 (московское время).<br>
						 Заказы, оформленные в будни после 16:00 и в выходные, обрабатываются утром ближайшего рабочего дня.<br>
						 Если вам не перезвонили – свяжитесь с нами,пожалуйста, по e-mail: <a href="mailto:ishop@fabrica-aztec.com">ishop@fabrica-aztec.com</a>.
					</p>
				</div>
			</div>
		</div>
		 <?
    }
?>
	</div>
	 <?
if(count($_SESSION["CART_ITEMS"])>0)
{
?> <?
}
?>
</form><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>