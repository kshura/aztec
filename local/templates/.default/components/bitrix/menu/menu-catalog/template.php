<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?if (!empty($arResult)):?>
<div class="row catalog-sections-nav">
	<div class="col-xs-12">
		<nav class="clear hidden-xs">
		<?
		foreach ($arResult as $arItem):
		?>
			<li><a href="<?=$arItem["LINK"]?>" class="<?if($arItem["SELECTED"]):?>akt<?endif;?>"><?=$arItem["TEXT"]?></a></li>
		<?
		endforeach;
		?>
		</nav>
		<div class="visible-xs">
			<div class="catalog-mobile-botton">
				<a href="javascritp:void(0);" data-toggle="collapse" data-target=".navbar-collapse-catalog">Каталог</a>
			</div>
			<nav class="clear navbar-collapse-catalog collapse">
			<?
			foreach ($arResult as $arItem):
			?>
				<li><a href="<?=$arItem["LINK"]?>"><?=$arItem["TEXT"]?></a></li>
			<?
			endforeach;
			?>	
			</nav>
		</div>
	</div>
</div>
<?endif?>
<?
//ppr($arResult);
?>