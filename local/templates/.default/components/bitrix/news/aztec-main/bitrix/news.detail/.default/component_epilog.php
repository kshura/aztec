<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
global $APPLICATION;
if($arResult['SECTION']['NAME'])
{
	$sect=" - ".$arResult['SECTION']['NAME']." - ".$APPLICATION->GetTitle("title");	
}
else
	$sect=" - ".$APPLICATION->GetTitle("title");
$APPLICATION->SetPageProperty("browser_title", $arResult["NAME"].$sect);
$APPLICATION->SetTitle($arResult["NAME"]);
?>