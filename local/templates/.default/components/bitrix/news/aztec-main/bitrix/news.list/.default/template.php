<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<h1><?=$arResult["NAME"]?></h1>
<div class="news-items">
    <div class="row">
    <?foreach($arResult["ITEMS"] as $arItem):?>
        <?
        $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
        $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
        ?>
        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-3 item" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
            <?if(is_array($arItem['PREVIEW_PICTURE'])):?>
            <div class="col-xs-12 img">
                <a href="<?=$arItem["DETAIL_PAGE_URL"]?>">
                    <img src="<?=$arItem['PREVIEW_PICTURE']['SRC']?>" alt="<?=$arItem['PREVIEW_PICTURE']['ALT']?>" class="img-responsive" />
                </a>
            </div>
            <?endif;?>
            <div class="col-xs-12">
                <a href="<?=$arItem["DETAIL_PAGE_URL"]?>" class="name"><?=$arItem["NAME"]?></a>
                <div class="date"><?=$arItem["DISPLAY_ACTIVE_FROM"]?></div>
                <div class="text"><?=$arItem["PREVIEW_TEXT"];?></div>
                <?if($arItem["PROPERTIES"]["SOURCE"]["VALUE"]):?>
                    <div class="source">
                        Источник: <a href="<?=$arItem["PROPERTIES"]["SOURCE"]["VALUE"]?>" target="_blank"><?=$arItem["PROPERTIES"]["SOURCE"]["DESCRIPTION"]?></a>
                    </div>
                <?endif;?>
            </div>
        </div>
    <?endforeach;?>
    </div>
</div>
<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
	<br /><?=$arResult["NAV_STRING"]?>
<?endif;?>

<?
//ppr($arResult);
?>