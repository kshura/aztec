<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<div class="row">
    <div class="col-xs-12">
        <div class="news-detail">
            <h1><?$APPLICATION->ShowTitle(false);?></h1>
            <div class="date"><?=$arResult['DISPLAY_ACTIVE_FROM']?></div>
            <div class="detail-text"><?=$arResult['DETAIL_TEXT']?></div>
        </div>
        <?if($arResult["PROPERTIES"]["SOURCE"]["VALUE"]):?>
            <div class="source">
                Источник: <a href="<?=$arResult["PROPERTIES"]["SOURCE"]["VALUE"]?>" target="_blank"><?=$arResult["PROPERTIES"]["SOURCE"]["DESCRIPTION"]?></a>
            </div>
        <?endif;?>
    </div>
</div>
<?
//ppr($arResult);
?>