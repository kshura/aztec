<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use \Bitrix\Main\Localization\Loc;
/**
 * @global CMain $APPLICATION
 * @var array $arParams
 * @var array $arResult
 * @var CatalogSectionComponent $component
 * @var CBitrixComponentTemplate $this
 * @var string $templateName
 * @var string $componentPath
 */

$this->setFrameMode(true);
?>
<div class="container-fluid recomend-block">
	<div class="row">
		<div class="col-xs-12 title">Также рекомендуем:</div>
	</div>
	<div class="row catalog-section">
	<?
	foreach($arResult["ITEMS"] as $key => $value):
	?>
		<div class="col-xs-12 col-sm-6 col-lg-3 item">
			<div class="img">
				<a href="<?=$value["DETAIL_PAGE_URL"]?>">
					<img src="<?=$value["PREVIEW_PICTURE"]["SRC"]?>" class="img-responsive" alt="<?=$value["PREVIEW_PICTURE"]["ALT"]?>">
				</a>
			</div>
			<div class="name">
				<?=$value["NAME"]?>
			</div>
			<div class="properties">
				<?foreach($value["DISPLAY_PROPERTIES"] as $prop_key => $prop):?>
					<?if($prop["VALUE"])
					{
					?>
						<div class=""><span><?=$prop["NAME"];?>: </span><?=$prop["VALUE"];?></div>
					<?
					}
					?>
				<?endforeach;?>
            </div>
            <?if($value["PROPERTIES"]["PRICE"]["VALUE"]):?>
                <div class="price">
                    Цена: <?=$value["PROPERTIES"]["PRICE"]["VALUE"]?> руб.
                </div>
                <div class="counter_block">
                    <div>
                        <a href="javascript:void(0);" class="minus">-</a>
                        <input type="text" value="1" onkeypress='validate(event)' />
                        <a href="javascript:void(0);" class="plus">+</a>
                    </div>
                </div>
                <div class="add-to-cart-block">
                    <span class="add-to-cart" data-detail="0" data-product-id="<?=$value["ID"]?>">в корзину</span>
                </div>
            <?endif;?>
		</div>
	<?
	endforeach;
	?>
	</div>
</div>
<?
//ppr($arResult);
?>
