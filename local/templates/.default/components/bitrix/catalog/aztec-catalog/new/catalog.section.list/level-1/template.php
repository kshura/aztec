<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

?>

<?
//ppr($arResult);
if($arResult["SECTION"]["DESCRIPTION"])
{
	?>
	<div class="row catalog-description">
		<div class="col-xs-12">
			<?=$arResult["SECTION"]["DESCRIPTION"];?>
		</div>
	</div>
	<?
}
?>
<?
if(count($arResult["SECTIONS"]))
{
?>
<div class="row catalog-section">
	<?foreach($arResult["SECTIONS"] as $key => $value):?>
		<div class="col-xs-12 col-sm-6 col-lg-3 item">
			<div class="img">
				<a href="<?=$value["SECTION_PAGE_URL"]?>">
					<img src="<?=$value["PICTURE"]["SRC"];?>" class="img-responsive" alt="<?=$value["PICTURE"]["ALT"]?>">
				</a>
			</div>
			<div class="name">
				<?=$value["NAME"]?>
			</div>
			<div class="text"><?=$value["UF_SMALL_TEXT"];?></div>
			<div class="more">
				<a href="<?=$value["SECTION_PAGE_URL"]?>">Подробнее</a>
			</div>
		</div>
	<?endforeach;?>
</div>
<?
}
?>
<?
//ppr($arResult);
?>