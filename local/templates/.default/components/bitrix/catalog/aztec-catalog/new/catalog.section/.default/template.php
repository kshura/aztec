<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<?
//echo count($arResult["ITEMS"]);
if(count($arResult["ITEMS"])==1)
{	
	LocalRedirect($arResult['ITEMS'][0]['DETAIL_PAGE_URL']);
}
echo $arResult["NAV_STRING"];
if(count($arResult["ITEMS"]))
{
?>
	<div class="row catalog-section">
	<?foreach($arResult["ITEMS"] as $key => $value):?>
		<div class="col-xs-12 col-sm-6 col-lg-3 item">
			<div class="img">
				<a href="<?=$value["DETAIL_PAGE_URL"]?>">
					<img src="<?=$value["DETAIL_PICTURE"]["SRC"]?>" class="img-responsive" alt="<?=$value["DETAIL_PICTURE"]["ALT"]?>">
				</a>
				<?if(count($value["PROPERTIES"]["MARKER"]["VALUE"])):?>
				<div class="label">
					<?foreach($value["PROPERTIES"]["MARKER"]["VALUE"] as $prop_key => $prop_value):?>
                        <div title="<?=$prop_value?>" >
                            <img src="/images/icon/<?=$value["PROPERTIES"]["MARKER"]["VALUE_XML_ID"][$prop_key]?>.png" class="img-responsive" alt="" />
                        </div>
					<?endforeach;?>
				</div>
				<?endif;?>
			</div>
			<div class="name">
				<?=$value["NAME"]?>
			</div>
			<div class="properties">
				<?foreach($value["DISPLAY_PROPERTIES"] as $prop_key => $prop):?>
					<?if($prop["VALUE"])
					{
					?>
						<div class=""><span><?=$prop["NAME"];?>: </span><?=$prop["VALUE"];?></div>
					<?
					}
					?>
				<?endforeach;?>
            </div>
            <?if($value["PROPERTIES"]["PRICE"]["VALUE"]):?>
                <div class="price">
                    Цена: <?=$value["PROPERTIES"]["PRICE"]["VALUE"]?> руб.
                </div>
                <div class="counter_block">
                    <div>
                        <a href="javascript:void(0);" class="minus">-</a>
                        <input type="text" value="1" onkeypress='validate(event)' />
                        <a href="javascript:void(0);" class="plus">+</a>
                    </div>
                </div>
                <div class="add-to-cart-block">
                    <span class="add-to-cart" data-detail="0" data-product-id="<?=$value["ID"]?>">в корзину</span>
                </div>
            <?endif;?>
		</div>
	<?endforeach;?>
	</div>
<?
}
?>
<?=$arResult["NAV_STRING"];?>
<div class="row catalog-description">
	<div class="col-xs-12">
		<?=htmlspecialcharsBack($arResult["UF_BOTTOM_DESCR"]);?>
	</div>
</div>
<?
//ppr($arResult);
?>