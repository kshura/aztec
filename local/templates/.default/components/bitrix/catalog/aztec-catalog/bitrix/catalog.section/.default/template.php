<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="row catalog-section">
<?foreach($arResult["ITEMS"] as $key => $value):?>
	<div class="col-xs-12 col-sm-6 col-lg-3 item">
		<div class="img">
			<img src="<?=$value["DETAIL_PICTURE"]["SRC"]?>" class="img-responsive" alt="<?=$value["DETAIL_PICTURE"]["ALT"]?>">
		</div>
		<div class="name">
			<?=$value["NAME"]?>
		</div>
		<div class="more">
			<a href="<?=$value["DETAIL_PAGE_URL"]?>">Подробнее</a>
		</div>
	</div>
<?endforeach;?>
</div>
<div class="row catalog-description">
	<div class="col-xs-12">
		Описание раздела сверху Описание раздела сверху Описание раздела сверху Описание раздела сверху Описание раздела сверху Описание раздела сверху Описание раздела сверху Описание раздела сверху Описание раздела сверху Описание раздела сверху Описание раздела сверху Описание раздела сверху Описание раздела сверху 
	</div>
</div>
<?
ppr($arResult);
?>