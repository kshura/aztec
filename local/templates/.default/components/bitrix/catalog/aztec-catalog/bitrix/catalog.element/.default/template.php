<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

	<div class="row element-detail">
		<div class="col-sm-5 col-lg-3 col-lg-offset-2 img-block">
			<div class="img">
                <img src="<?=$arResult["PREVIEW_PICTURE"]["SRC"]?>" class="img-responsive" alt="<?=$arResult["PREVIEW_PICTURE"]["ALT"]?>">
                <?if(count($arResult["PROPERTIES"]["MARKER"]["VALUE"])):?>
				<div class="label">
					<?foreach($arResult["PROPERTIES"]["MARKER"]["VALUE"] as $prop_key => $prop_value):?>
                        <div title="<?=$prop_value?>" >
                            <img src="/images/icon/<?=$arResult["PROPERTIES"]["MARKER"]["VALUE_XML_ID"][$prop_key]?>.png" class="img-responsive" alt="" />
                        </div>
					<?endforeach;?>
				</div>
				<?endif;?>
			</div>
			<?if(count($arResult["PROPERTIES"]["PHOTO"]["VALUE"])):?>
			<div class="row">
				<div class="col-xs-12 small-photo hidden-xs">	
					<?foreach($arResult["PROPERTIES"]["PHOTO"]["VALUE"] as $key=>$value):?>				
						<a href="<?=CFile::GetPath($value);?>" data-fancybox="images" data-caption="<?=$arResult["PROPERTIES"]["PHOTO"]["DESCRIPTION"][$key]?>">
							<?
							$resize_foto = CFile::ResizeImageGet($value, array('width'=>100, 'height'=>100), BX_RESIZE_IMAGE_ALT, true);
							?>
							<img 
								src="<?=$resize_foto['src']?>" 
								alt="<?=$arResult["PROPERTIES"]["PHOTO"]["DESCRIPTION"][$key]?>"
								class="img-responsive"
							/>
						</a>
					<?endforeach;?>
				</div>
			</div> 
			<?endif;?>
		</div>
		<div class="col-sm-offset-1 col-sm-6 col-lg-offset-1 col-lg-5">
			<div class="name"><?$APPLICATION->ShowTitle(false);?></div>
            <?if($arResult["PROPERTIES"]["PRICE"]["VALUE"]):?>
                <div class="price-cart-block">
                    <div class="price">
                        Цена: <?=$arResult["PROPERTIES"]["PRICE"]["VALUE"]?> руб.
                    </div>
                    <div class="counter_block">
                        <div>
                            <a href="javascript:void(0);" class="minus">-</a>
                            <input type="text" value="1" onkeypress='validate(event)' />
                            <a href="javascript:void(0);" class="plus">+</a>
                        </div>
                    </div>
                    <div class="add-to-cart-block">
                        <span class="add-to-cart" data-detail="1" data-product-id="<?=$arResult["ID"]?>">в корзину</span>
                    </div>
                </div>
            <?endif;?>
            <?if($arResult["PROPERTIES"]["ARTICUL"]["VALUE"]):?>
				<div class="weight"><b>Артикул:</b> <?=$arResult["PROPERTIES"]["ARTICUL"]["VALUE"]?></div>
			<?endif;?>
			<?if($arResult["PROPERTIES"]["WEIGHT"]["VALUE"]):?>
				<div class="weight"><b>Вес:</b> <?=$arResult["PROPERTIES"]["WEIGHT"]["VALUE"]?></div>
			<?endif;?>
			<?if($arResult["PROPERTIES"]["SIZE"]["VALUE"]):?>
				<div class="weight"><b>Размеры упаковки:</b> <?=$arResult["PROPERTIES"]["SIZE"]["VALUE"]?></div>
			<?endif;?>
			<?if($arResult["PROPERTIES"]["GOFR"]["VALUE"]):?>
				<div class="weight"><b>В гофрокоробе:</b> <?=$arResult["PROPERTIES"]["GOFR"]["VALUE"]?></div>
			<?endif;?>
			<?if($arResult["PROPERTIES"]["SHOW"]["VALUE"]):?>
				<div class="weight"><b>В шоубоксе:</b> <?=$arResult["PROPERTIES"]["SHOW"]["VALUE"]?></div>
			<?endif;?>
			<?if($arResult["PROPERTIES"]["EXPIRATION"]["VALUE"]):?>
				<div class="weight"><b>Срок годности:</b> <?=$arResult["PROPERTIES"]["EXPIRATION"]["VALUE"]?></div>
			<?endif;?>
			<?if($arResult["PROPERTIES"]["CONSIST"]["VALUE"]):?>
				<div class="weight"><b>Состав:</b> <?=htmlspecialcharsBack($arResult["PROPERTIES"]["CONSIST"]["VALUE"]);?></div>
			<?endif;?>
			<?if($arResult["DETAIL_TEXT"]):?>
				<div class="description">
					<?=$arResult["DETAIL_TEXT"]?>
				</div>
			<?endif;?>
		</div>
	</div>
</div>
<?
$GLOBALS["RECOMEND"] = $arResult["PROPERTIES"]["ALSO"]["VALUE"];
//ppr($arResult);
?>