<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

?>
<div class="row catalog-sections-nav">
	<div class="col-xs-12">
		<nav class="clear hidden-xs">
		<?
		foreach ($arResult['SECTIONS'] as &$arSection):
		?>
			<li><a href="<?=$arSection["SECTION_PAGE_URL"];?>"><?=$arSection["NAME"];?></a></li>
		<?
		endforeach;
		?>
		</nav>
		<div class="visible-xs">
			<div class="catalog-mobile-botton">
				<a href="javascritp:void(0);" data-toggle="collapse" data-target=".navbar-collapse-catalog">Каталог</a>
			</div>
			<nav class="clear navbar-collapse-catalog collapse">
			<?
			foreach ($arResult['SECTIONS'] as &$arSection):
			?>
				<li><a href="<?=$arSection["SECTION_PAGE_URL"];?>"><?=$arSection["NAME"];?></a></li>
			<?
			endforeach;
			?>	
			</nav>
		</div>
	</div>
</div>
<?
//ppr($arResult);
?>
