<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="row catalog-sections">
<?foreach($arResult["ITEMS"] as $key => $arItem):?>
	<div class="col-xs-6 col-lg-3 item-div" style="background-image:url(<?=$arItem['PREVIEW_PICTURE']['SRC']?>)">
		<div class="item">
			<span><?=$arItem['NAME']?></span>
			<div class="hover" style="background-image: url(<?=CFile::GetPath($arItem['PROPERTIES']['FON']['VALUE']);?>);">
				<div class="name"><?=$arItem['NAME']?></div>
				<div class="text"><?=$arItem['PREVIEW_TEXT']?></div>
				<div class="more"><a href="<?=$arItem['PROPERTIES']['LINK']['VALUE']?>">Подробнее</a></div>
			</div>
			<a href="<?=$arItem['PROPERTIES']['LINK']['VALUE']?>" class="gadget-link"></a>
		</div>		
	</div>
<?endforeach;?>
</div>

