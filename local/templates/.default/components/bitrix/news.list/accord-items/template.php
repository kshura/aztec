<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<?if(count($arResult["ITEMS"])):?>
	<ul class="accord-items photo">
	<?foreach($arResult["ITEMS"] as $arItem):?>
		<li class="item">
			<div class="name"><?=$arItem["NAME"]?></div>
			<div class="contacts"><?=$arItem["PREVIEW_TEXT"]?></div>
		</li>
	<?endforeach;?>
	</ul>
<?endif;?>

