<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="row banners-xs-sm-md hidden-lg">
	<?foreach($arResult["ITEMS"] as $key => $arItem):?>
		<div class="item col-xs-12 col-sm-6">
			<div class="item-cell banner-<?=$key+1;?>" style="background-image:url(<?=CFile::GetPath($arItem['PROPERTIES']['PIC1']['VALUE']);?>)">
				<a href="<?=$arItem['PROPERTIES']['LINK']['VALUE']?>" class="gadget-link">
				</a>
				<div class="main-hover" style="background-image: url(<?=CFile::GetPath($arItem['PROPERTIES']['PIC2']['VALUE']);?>)">
					<div class="inner" style="background-color:#fdfdfd; background-image: url(<?=CFile::GetPath($arItem['PROPERTIES']['PIC3']['VALUE']);?>)">
						<img src="<?=CFile::GetPath($arItem['PROPERTIES']['PIC4']['VALUE']);?>" class="img-responsive" alt="" />
						<div class="more">
							<a href="<?=$arItem['PROPERTIES']['LINK']['VALUE']?>">← В КАТАЛОГ</a><br /><br />
							<a href="<?=$arItem['PROPERTIES']['LINKSITE']['VALUE']?>" target="_blank">НА САЙТ МАРКИ  →</a>				
						</div>
					</div>
				</div>                    
			</div>
		</div>
	<?endforeach;?>
</div>

