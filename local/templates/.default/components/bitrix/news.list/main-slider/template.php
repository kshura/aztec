<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div id="carousel-example-generic" class="carousel slide hidden-xs" data-ride="carousel">
	<ol class="carousel-indicators">
		<?
		$i=0;
		foreach($arResult["ITEMS"] as $arItem):?>
			<li data-target="#carousel-example-generic" data-slide-to="<?=$i?>" class="<?if(!$i):?>active<?endif;?>"></li>
		<?
		$i++;
		endforeach;?>
	</ol>
	<div class="carousel-inner" role="listbox">
		<?
		$i=0;
		foreach($arResult["ITEMS"] as $arItem):?>
		<div class="item<?if(!$i):?> active<?endif;?>">
			<?if($arItem["PROPERTIES"]["LINK"]["VALUE"])
			{
			?>
				<a href="<?=$arItem["PROPERTIES"]["LINK"]["VALUE"]?>" <?if($arItem["PROPERTIES"]["NEW"]["VALUE"]):?>target="_blank"<?endif;?>>
					<img src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>" class="img-responsive" alt="<?=$arItem["PREVIEW_PICTURE"]["ALT"]?>">
				</a>
			<?
			}
			else
			{
			?>
				<img src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>" class="img-responsive" alt="<?=$arItem["PREVIEW_PICTURE"]["ALT"]?>">
			<?
			}
			?>
		</div>
		<?
		$i++;
		endforeach;?>
	</div>
</div>
<?
//ppr($arResult);
?>