<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?header('Content-type: text/html; charset=utf-8');?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
    <meta property="og:image" content="http://<?=$_SERVER["HTTP_HOST"]?>/images/ATsTEK_logo_250kh250_png_prozr.png" />
    <meta property="og:image:type" content="image/png" />
    <meta property="og:url" content="http://<?=$_SERVER["HTTP_HOST"]?><?=$_SERVER["REQUEST_URI"]?>">
	<link rel="icon" href="/favicon.ico" type="image/x-icon">
	<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon"> 
    <title><?$APPLICATION->ShowTitle()?></title>
    <?
    $APPLICATION->ShowHead();
    $APPLICATION->SetAdditionalCSS('/css/bootstrap.css');
	$APPLICATION->SetAdditionalCSS('/css/font-awesome.css');
	$APPLICATION->SetAdditionalCSS('/css/jquery.fancybox.min.css');
	$APPLICATION->SetAdditionalCSS('/css/styles.css');
	$APPLICATION->SetAdditionalCSS('/css/additional_styles.css');
    $APPLICATION->AddHeadScript('https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js');
	$APPLICATION->AddHeadScript('/js/bootstrap.min.js');
	$APPLICATION->AddHeadScript('/js/jquery.fancybox.min.js');
	$APPLICATION->AddHeadScript('/js/jquery.mb.browser.min.js');
	$APPLICATION->AddHeadScript('/js/script.js');
	$APPLICATION->AddHeadScript('/js/script_additional.js');
    ?>
</head>
<body>
<? $APPLICATION->ShowPanel();?>
	<div class="shell">
    <div class="cart-mobile-div">
        <?	
        $APPLICATION->IncludeFile(
            SITE_DIR."include/top_cart_mobile.php",
            Array(),
            Array("MODE"=>"text")
        );
        ?>
    </div>
	<header class="hidden-xs">
		<div class="menu-opener">
			<button type="button" class="navbar-toggle">
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>

				<span class="glyphicon glyphicon-remove"></span>
			</button>
		</div>
		<a href="/" class="logo">
			<img src="/images/logo.png" alt="" class="img-responsive" />
		</a>
		<?$APPLICATION->IncludeComponent(
			"bitrix:menu", 
			"left-menu", 
			array(
				"ALLOW_MULTI_SELECT" => "N",
				"CHILD_MENU_TYPE" => "left",
				"COMPONENT_TEMPLATE" => "left-menu",
				"DELAY" => "N",
				"MAX_LEVEL" => "2",
				"MENU_CACHE_GET_VARS" => array(
				),
				"MENU_CACHE_TIME" => "3600",
				"MENU_CACHE_TYPE" => "N",
				"MENU_CACHE_USE_GROUPS" => "Y",
				"ROOT_MENU_TYPE" => "top",
				"USE_EXT" => "Y"
			),
			false
		);?>
        <div class="top-cart">
            <?	
            $APPLICATION->IncludeFile(
                SITE_DIR."include/top_cart.php",
                Array(),
                Array("MODE"=>"text")
            );
            ?>
        </div>
		<div class="phone-form-block">
			<?
			$APPLICATION->IncludeFile(
				SITE_DIR."include/phone.php",
				Array(),
				Array("MODE"=>"html")
			);
			?>
			<?
			$APPLICATION->IncludeFile(
				SITE_DIR."include/call-back-botton.php",
				Array(),
				Array("MODE"=>"html")
			);
			?>
		</div>
	</header>
	<div class="container-fluid">
		<div class="row">
			<div class="visible-xs col-xs-12 top-menu-mobile">
				<a href="/" class="logo"><img src="/images/logo.png" alt="" /></a>
				<a class="navbar-brand visible-xs" href="javascritp:void(0);" data-toggle="collapse" data-target=".navbar-collapse">
					Меню
				</a>
				<div class="phone">
					<?
					$APPLICATION->IncludeFile(
						SITE_DIR."include/phone.php",
						Array(),
						Array("MODE"=>"html")
					);
					?>
				</div>                                    
				<div class="clear"></div>
				<div class="navbar-collapse collapse">                            
					<?$APPLICATION->IncludeComponent(
						"bitrix:menu", 
						"left-menu-mobile", 
						array(
							"ALLOW_MULTI_SELECT" => "N",
							"CHILD_MENU_TYPE" => "left",
							"COMPONENT_TEMPLATE" => "left-menu",
							"DELAY" => "N",
							"MAX_LEVEL" => "2",
							"MENU_CACHE_GET_VARS" => array(
							),
							"MENU_CACHE_TIME" => "3600",
							"MENU_CACHE_TYPE" => "N",
							"MENU_CACHE_USE_GROUPS" => "Y",
							"ROOT_MENU_TYPE" => "top",
							"USE_EXT" => "Y"
						),
						false
					);?>
				</div>
			</div>
		</div>
	</div>
<div class="main">
    <div class="container-fluid">
		<div class="row">
			<div class="col-xs-6 visible-lg pic-block">
				<?$APPLICATION->IncludeComponent("bitrix:main.include", "", array());?> 
            </div>
            <div class="col-xs-12 col-lg-6 content">